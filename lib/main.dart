import 'package:digi_style/profileTab.dart';
import 'package:digi_style/views/SearchTab/searchTab.dart';
import 'package:digi_style/views/homeTab/homeTabPage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'addToCardTab.dart';

import 'moreTab.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(


      title: 'Digistyle',
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: [
          Locale("fa", "IR"), // OR Locale('ar', 'AE') OR Other RTL locales
        ],
//        locale: Locale("fa", "IR"),
        theme: ThemeData(
          textTheme: TextTheme(
          display1: TextStyle(fontSize: 14, fontWeight: FontWeight.bold,color: Theme.of(context).textSelectionHandleColor),
//            body2: ,
          //title is used for bottom button navigation
            title: TextStyle(fontSize: 14, fontWeight: FontWeight.bold,color: Theme.of(context).textSelectionHandleColor),

//            caption:  ,
//            button: ,
        ),
        backgroundColor: Color.fromRGBO(234, 230, 234,1.0),
        fontFamily: "Yekan",
        accentColor: Color.fromRGBO(255, 85, 136,1.0),
        textSelectionColor: Color.fromRGBO(100, 98, 99,1.0),
        textSelectionHandleColor: Color.fromRGBO(255, 255, 255,1.0),
        accentIconTheme: IconThemeData(color: Color.fromRGBO(255, 85, 136,1.0),size: 32.0),
        iconTheme: IconThemeData(color: Color.fromRGBO(183, 132, 145,1.0),size: 32.0),
        cardColor:  Color.fromRGBO(208, 204, 204,1.0),
        appBarTheme: AppBarTheme(
          color: Color.fromRGBO(26, 25, 19,1.0)
        ),
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'DIGISTYLE'),
        routes: {
          '/homeTab': (context) => new HomeTab(),
          '/searchTab': (context) => new SearchTab(),
          '/addToCardTab': (context) => new AddToCardTab(),
          '/profileTab': (context) => new ProfileTab(),
          '/moreTab': (context) => new MoreTab(),
        }
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _selectedIndex = 0;
  final _widgetOptions = [
    HomeTab(),
    SearchTab(),

  Container(
      child: Text("Home Body"),
    ),
    Container(
      child: Text("Home Body"),
    ),
    Container(
      child: Text("Home Body"),
    ),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        selectedIconTheme: Theme.of(context).accentIconTheme,
        unselectedIconTheme: Theme.of(context).iconTheme,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home,),
            activeIcon:Icon(Icons.home,) ,
            title: _selectedIndex==0?Text("خانه",textAlign: TextAlign.center,style: TextStyle(fontWeight:FontWeight.bold,color:Theme.of(context).accentColor , fontSize: 14.0),):Container(),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.search,),
            activeIcon:Icon(Icons.search,) ,
            title: _selectedIndex==1?Text("جستجو",textAlign: TextAlign.center, style: TextStyle( fontWeight:FontWeight.bold,color:Theme.of(context).accentColor , fontSize: 14.0),):Container(),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.local_grocery_store,),
            activeIcon:Icon(Icons.local_grocery_store,) ,
            title: _selectedIndex==2?Text("سبد خرید",textAlign: TextAlign.center,style: TextStyle(fontWeight:FontWeight.bold,color:Theme.of(context).accentColor , fontSize: 14.0),):Container(),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person_outline,),
            activeIcon:Icon(Icons.person,) ,
            title: _selectedIndex==3?Text("پروفایل",
              style: TextStyle(color:Theme.of(context).accentColor ,fontWeight:FontWeight.bold, fontSize: 14.0),
              textAlign: TextAlign.center,)
                :Container(),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.markunread_mailbox,),
            activeIcon:Icon(Icons.markunread_mailbox, ) ,
            title: _selectedIndex==4?Text("بیشتر",style: TextStyle(fontWeight:FontWeight.bold,color:Theme.of(context).accentColor , fontSize: 14.0),textAlign: TextAlign.center,):Container(),
          ),

        ],
        currentIndex: _selectedIndex,
        fixedColor: Color(0xffff8800),
        type: BottomNavigationBarType.fixed,
        onTap: _onItemTapped,
      )
  );
  }
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
}
