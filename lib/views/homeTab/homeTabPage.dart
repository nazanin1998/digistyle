
import 'package:digi_style/views/homeTab/digiStyle.dart';
import 'package:digi_style/views/homeTab/genderTab.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomeTab extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => HomeTabState();

}

class HomeTabState extends State<HomeTab>{
  @override
  Widget build(BuildContext context) {
    double width = 78.0;
    return Scaffold(

      appBar: AppBar(
        backgroundColor: Theme.of(context).appBarTheme.color,
        title: Text("DIGISTYLE", style: TextStyle(fontSize: 16),),
      ),
      body: DefaultTabController(initialIndex: 0,

        length: 5,
        child: Column(
          children: <Widget>[
            Container(
                constraints: BoxConstraints.expand(height: 43),
                child: Material(
                  color: Theme.of(context).appBarTheme.color,
                  child: TabBar(

                    isScrollable: true,
                    tabs: [
                      Tab(
                        child: Container(
                          alignment: Alignment.center,
                          width:  width,
                          child: Text("دیجی استایل"),
                        ),
                      ),
                      Tab(
                        child: Container(
                          alignment: Alignment.center,
                          width:  width,
                          child: Text("مردانه",),
                        ),
                      ),

                      Tab(
                        child: Container(
                          alignment: Alignment.center,
                          width:  width,
                          child: Text("زنانه",),
                        ),
                      ),
                      Tab(
                        child: Container(
                          alignment: Alignment.center,
                          width:  width,
                          child: Text("بچگانه",),
                        ),
                      ),
                      Tab(
                        child: Container(
                          alignment: Alignment.center,
                          width:  width,
                          child: Text("کارت هدیه",),
                        ),
                      ),

                    ],

                    labelColor: Theme.of(context).accentColor,
                    labelPadding: EdgeInsets.symmetric(vertical: 10),
                    labelStyle: TextStyle(fontSize: 14.0,fontWeight:FontWeight.bold,fontFamily: "Yekan"),
                    unselectedLabelColor: Theme.of(context).textSelectionHandleColor,
                    unselectedLabelStyle: TextStyle(fontSize: 14.0,fontWeight:FontWeight.bold,fontFamily: "Yekan"),
                  ),
                )
            ),
            Expanded(
              child: Container(
                child: TabBarView(children: [
                  DigiStyle(),
                  genderTab('assets/images/men1.jpg','assets/images/men2.jpg','assets/images/men3.jpg','assets/images/men4.jpg','assets/images/men5.jpg',"جدید ترین مردانه","RNS","...تیشرت مخصوص ","1,500,000 تومان", "2,222,000 تومان",1),
                  genderTab('assets/images/women1.jpg','assets/images/women2.jpg','assets/images/women3.jpg','assets/images/women4.jpg','assets/images/women5.jpg',"جدید ترین زنانه","Aseman ceramic","...گوشواره رنانه آسما ","300,000 تومان", "122,000 تومان",2),
                  genderTab('assets/images/children1.jpg','assets/images/children2.jpg','assets/images/children3.jpg','assets/images/children4.jpg','assets/images/children5.jpg',"جدید ترین بچگانه","narbon","...ست تیشرت و شلوار","104,000 تومان", "99,000 تومان",3),
                  ExpansionTile(),

                ]),
              ),
            )
          ],
        ),
      ),

    );
  }
  Widget ExpansionTile()
  {
    return Container(
      color: Theme.of(context).backgroundColor,
      child:
      Column(children:<Widget>[Card(color: Colors.white,child: ListView.builder(

        shrinkWrap: true,
        itemBuilder: (BuildContext context, int index) =>
            Directionality(child: EntryItem(data1[index]) ,textDirection:TextDirection.rtl,),

        itemCount: data1.length,
      ),)]
        ,),
    );


  }

}  final List<Entry> data1 = <Entry>[
  Entry(
    'کارت هدیه دیجی استایل',
    <Entry>[
      Entry('کارت هدیه دیجی استایل',),
      Entry(' مشاهده کارت هدیه دیجی استایل'),
      Entry('پیراهن'),
    ],
  ),
];


class Entry {
  Entry(this.title, [this.children = const <Entry>[]]);

  final String title;
  final List<Entry> children;
}

// The entire multilevel list displayed by this app.

// Displays one Entry. If the entry has children then it's displayed
// with an ExpansionTile.
class EntryItem extends StatelessWidget {
  const EntryItem(this.entry);

  final Entry entry;

  Widget _buildTiles(Entry root) {
    if (root.children.isEmpty) return ListTile(title: Text(root.title));
    return ExpansionTile(
      key: PageStorageKey<Entry>(root),
      title: Text(root.title),
      children: root.children.map(_buildTiles).toList(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildTiles(entry);
  }
}