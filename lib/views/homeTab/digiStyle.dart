import 'package:carousel_slider/carousel_slider.dart';
import 'package:digi_style/views/homeTab/productCard.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'GenderCard.dart';

class DigiStyle extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => DigiStyleState();


}

class DigiStyleState extends State<DigiStyle>{
  int _current = 0;
  final List<String> slideImgList = [
    'assets/images/slide1.jpg',
    'assets/images/slide2.jpg',
    'assets/images/slide3.jpg',
    'assets/images/slide4.jpg',
    'assets/images/slide5.jpg',
    'assets/images/slide6.jpg',
  ];
  final List<String> brandImgList = [
    'assets/images/brand1.png',
    'assets/images/brand2.png',
    'assets/images/brand3.png',
    'assets/images/brand4.png',
    'assets/images/brand5.png',
    'assets/images/brand1.png',
    'assets/images/brand2.png',
    'assets/images/brand3.png',
    'assets/images/brand4.png',
    'assets/images/brand5.png',
  ];
  final List<String> genderList = [
    'مردانه',
    'بچگانه',
    'زنانه',
  ];
  final List<String> genderImgList = [
    'assets/images/men.jpg',
    'assets/images/kid.jpg',
    'assets/images/woman.jpg',
  ];

  @override
  Widget build(BuildContext context) {

    return Container(
      color: Theme.of(context).backgroundColor,
      child: SingleChildScrollView(

        child: Column(children: [
          mySlider(),
          brandRow(),
          horizontalProductList("پیشنهاد دیجی استایل", "آدیداس", "...کفش مخصوص دویدن", "3,500,000 تومان", "3,222,000 تومان", 'assets/images/cloth2.jpg', false),
          horizontalGenderList(),
          animatedPicture(),
          horizontalProductList("پربازدیدترین های دیجی استایل", "آدیداس", "...کاپشن ورزشی مردانه", "3,900,000 تومان", "3,800,000 تومان", 'assets/images/porbazdidtarin.jpg', true),
          horizontalProductList("جدیدترین های دیجی استایل", "منگو", "...پلیور مردانه جدید", "7,900,000 تومان", "7,800,000 تومان", 'assets/images/men5.jpg', true),

        ]),
      ),
    );
  }

  Widget animatedPicture(){
    return Container(
      margin: EdgeInsets.only(top: 15.0),
      height: 100.0,
      color: Theme.of(context).cardColor,
      width: MediaQuery.of(context).size.width,
      child: Center(child: Card(
          elevation: 1.0,
          color: Colors.white,
          shape:  RoundedRectangleBorder(borderRadius: BorderRadius.circular(2.0)),
          child: Container(
            height: 45.0,
            width: 170.0,
            child: new Center(
              child: Text("فروش ویژه دیجی استایل", style: TextStyle(fontWeight:FontWeight.bold,fontSize: 14.0,),),
            ),
          )    )));
  }
  Widget horizontalGenderList(){
    return Container(

      margin: EdgeInsets.only(top: 20.0, left: 2.0, right: 2.0 ),
      height: 300.0,
      child: ListView.builder(
          reverse: true,
          scrollDirection: Axis.horizontal,
          itemCount: 3,
          itemBuilder: (context, index) {
            if (index >= 3)
              return null;
            return GenderCard(genderList[index],genderImgList[index]);
          }
      ),
    );
  }

  Widget horizontalProductList(String title ,String name, String des, String price, String off, String asset, bool hasAll){
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            hasAll?Padding(padding: EdgeInsets.only(left:15.0, right: 15.0 ,bottom: 8.0, top:15.0 ),child:Text("همه",style: TextStyle(color: Color.fromRGBO(67, 202, 211, 1.0) , fontSize: 16.0)) ,):Container(),

            Expanded(child: Row(),),
            Padding(padding: EdgeInsets.only(left:15.0, right: 15.0 ,bottom: 8.0, top:15.0 ),child:Text(title, style: TextStyle( fontSize: 16.0,color: Theme.of(context).textSelectionColor),) ,)
          ],
        ),
        Container(
          padding: EdgeInsets.only(right: 5.0, left: 5.0),
          height: 300.0,

          child: ListView.builder(
              padding:EdgeInsets.all(0.0),

              reverse: true,
              scrollDirection: Axis.horizontal,
              itemCount: 10,
              itemBuilder: (context, index) {
                if (index >= 10)
                  return null;
                return ProductCard(name: name,des: des,price: price,off: off, asset: asset,);
              }
          ),
        )
      ],
    );
  }

  Widget brandRow(){
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Padding(padding: EdgeInsets.all(18.0),child: Text("همه",style: TextStyle(color: Color.fromRGBO(67, 202, 211, 1.0) , fontSize: 16.0),),),
            Expanded(child: Row(),),
            Padding(padding: EdgeInsets.all(18.0),child: Text("برندها",style: TextStyle(color: Theme.of(context).textSelectionColor , fontSize: 16.0),),),
          ],
        ),
        Container(
          height: 67.0,
          color: Colors.white,
          child: ListView.builder(
            reverse: true,
              scrollDirection: Axis.horizontal,
              itemCount: 10,
              itemBuilder: (context, index) {
                if (index >= 10)
                  return null;
                return Container(
                  height: 60.0,
                  width: 70.0,
                  child:Image.asset(brandImgList[index], fit: BoxFit.contain,height: 50.0,width: 50.0),
                );
              }
          ),
        )

      ],
    );
  }
  Widget mySlider(){
    return Stack(
      alignment: Alignment.bottomCenter,
      children: <Widget>[

        getFullScreenCarousel(context),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: map<Widget>(
            slideImgList,
                (index, url) {
              return Container(
                width: 9.0,
                height: 9.0,
                margin: EdgeInsets.symmetric(vertical: 15.0, horizontal: 5.0),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: _current == 5 - index
                        ? Theme.of(context).accentColor
                        : Theme.of(context).backgroundColor),
              );
            },
          ),
        ),
      ],
    );
  }
  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }

    return result;
  }
  //create full screen Carousel with context
  CarouselSlider getFullScreenCarousel(BuildContext mediaContext) {
    return CarouselSlider(

      height: 220.0,
      reverse: true,
      autoPlay: false,
      viewportFraction: 1.0,
      items: slideImgList.map(
            (address) {
          return Container(
            child: ClipRRect(

              borderRadius: BorderRadius.all(Radius.circular(0.0)),
              child:   Image.asset(address, fit: BoxFit.cover,width: MediaQuery.of(context).size.width,)

            ),
          );
        },
      ).toList(),
      onPageChanged: (index) {
        setState(() {
          _current = index;
        });
      },
    );
  }
}