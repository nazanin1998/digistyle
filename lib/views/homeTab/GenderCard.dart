import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class GenderCard extends StatelessWidget{
  String title;
  String assets;
  GenderCard(this.title, this.assets);
  @override
  Widget build(BuildContext context) {
      return new Container(
          height: 220.0,
          width: 300.0,
        child: Card(
          elevation: 2.0,
//
//
            color: Theme.of(context).cardColor,
          child:new ClipRRect(
            borderRadius: new BorderRadius.circular(3.0),
            child: Container(
                child:
                AspectRatio(
                  aspectRatio: 16 / 9,
                  child: Column(
                    children: <Widget>[
                      Container(
                          width: MediaQuery.of(context).size.width,
                          child: Image.asset(assets, fit: BoxFit.cover, height: 240.0,width: 300.0,),),

                      Padding(padding: EdgeInsets.all(10.0),child:Text(" < "+title, style: TextStyle(fontSize: 18.0,fontWeight: FontWeight.bold),) ,)
                    ],
                  )

                )),
          )

        ),
        );




//    Container(
//
//        height: 220.0,
//        width: 350.0,
//        child: Card(
//
//
//            color: Theme.of(context).cardColor,
//          child:Column(
//            children: <Widget>[
//              Image.asset(assets, fit: BoxFit.fill, height: 240.0,width: 350.0,),
//              Padding(padding: EdgeInsets.all(10.0),child:Text(" < "+title, style: TextStyle(fontSize: 18.0,fontWeight: FontWeight.bold),) ,)
//            ],
//          )
//
//        ),
//      );
  }

}