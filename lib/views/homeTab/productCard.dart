
import 'package:digi_style/views/ProductSingle/productSingle.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProductCard extends StatelessWidget{
  String name;
  String des;
  String price;
  String off;
  String asset;
  bool colorchange;
  ProductCard({this.name="آدیداس", this.des="...کفش مخصوص دویدن",this.price="3,500,000 تومان",this.off="3,322,000 تومان",this.asset='assets/images/cloth2.jpg',this.colorchange=false});
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        Navigator.of(context).push(MaterialPageRoute(builder: (context)=>ProductSingle(name, des, price, off, asset)));
      },
      child: Container(
        width: 155.0,
        child: Card(
          elevation: 1.0,
          shape:RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(2.0),
          ) ,
          color: Colors.white,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Container(
                margin:EdgeInsets.only(left: 10.0,right: 10.0),
                height: 200.0,
                width: 150.0,
                child:Image.asset(asset, fit: BoxFit.contain),
              ),

              Container(
                  width: 155.0,

                  child:Column(
                    crossAxisAlignment:CrossAxisAlignment.end,
                    children: <Widget>[
                      Padding(padding: EdgeInsets.only(right: 13.0,left: 13.0),child: Text(name,style: TextStyle(fontSize: 14.0))),
                      Padding(padding: EdgeInsets.only(right: 13.0,left: 13.0),child: Text(des,style: TextStyle(fontSize: 14.0),),),

                    ],)
              ),


              Container(
                  width: 150.0,
                  child:Column(
                    crossAxisAlignment:CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(padding: EdgeInsets.only(left: 13.0, top: 8.0),child: Text(price, textDirection:TextDirection.rtl,style: TextStyle(decoration: TextDecoration.lineThrough,fontSize:11.0,color:Theme.of(context).textSelectionColor ),),),
                      Padding(padding: EdgeInsets.only(left: 13.0),child: Text(off, textDirection:TextDirection.rtl,style: TextStyle(color:colorchange?Theme.of(context).textSelectionColor :Theme.of(context).accentColor, fontSize: 12.0),),),

                    ],)
              ),

            ],
          ),
        ),
      ),
    );
  }

}