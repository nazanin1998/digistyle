
import 'package:digi_style/views/homeTab/productCard.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
class genderTab extends StatefulWidget{
  String picture1;
  String picture2;
  String picture3;
  String picture4;
  String picture5;
  String horizentalTitle;
  String name ;
  String desc ;
  String price ;
  String off;
int gender ;
  genderTab(this.picture1,this.picture2,this.picture3,this.picture4,this.picture5,this.horizentalTitle,this.name,this.desc,this.price,this.off,this.gender);

  @override
  State<StatefulWidget> createState() => genderTabState(picture1,picture2,picture3,picture4,picture5,horizentalTitle,name,desc,price,off,gender);


}

class genderTabState extends State<genderTab> {
  String picture1;
  String picture2;
  String picture3;
  String picture4;
  String picture5;
  String horizentalTitle;
  String name ;
  String desc ;
  String price ;
  String off;
  int gender;
  genderTabState(this.picture1,this.picture2,this.picture3,this.picture4,this.picture5,this.horizentalTitle,this.name,this.desc,this.price,this.off,this.gender);
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).backgroundColor,
      child: SingleChildScrollView(

        child: Column(children: [
Padding(child: ImageCard(location:picture1),padding:EdgeInsets.only(top:4) ,),

          ExpansionTile(),
          ImageCard(location:picture2),
          ImageCard(location:picture3),
          ImageCard(location:picture4),
          horizontalProductList(horizentalTitle, name, desc, price, off, picture5),

        ]),
      ),
    );
  }
  Widget ImageCard({String location='assets/images/men1.jpg'})
  {
    return new Container(height:170.0,child: Card(
      elevation: 2.0,
        child:new ClipRRect(
          borderRadius: new BorderRadius.circular(3.0),
          child: Container(
              child:
              AspectRatio(
                aspectRatio: 25 / 9,
                child: Container(
                    width: MediaQuery.of(context).size.width,
                    child:Image.asset(location,fit: BoxFit.fill,) ,),)

              )),
        ));
  }
  Widget ExpansionTile()
  {
    if(gender==1){
      return
        Card(child: ListView.builder(
          shrinkWrap: true,
          itemBuilder: (BuildContext context, int index) =>
              Directionality(child: EntryItem(data1[index]) ,textDirection:TextDirection.rtl,),

          itemCount: data1.length,
        ),);
    }
      else if(gender==2){
      return
        Card(child: ListView.builder(
          shrinkWrap: true,
          itemBuilder: (BuildContext context, int index) =>
              Directionality(child: EntryItem(data2[index]) ,textDirection:TextDirection.rtl,),

          itemCount: data2.length,
        ),);
    }
        else if (gender==3){
      return
        Card(color:Colors.white,child: ListView.builder(
          shrinkWrap: true,
          itemBuilder: (BuildContext context, int index) =>
              Directionality(child: EntryItem(data3[index]) ,textDirection:TextDirection.rtl,),

          itemCount: data3.length,
        ),);
    }

  }

  Widget horizontalProductList(String title ,String name, String des, String price, String off, String asset){
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Expanded(child: Row(),),
            Padding(padding: EdgeInsets.only(left:15.0, right: 15.0 ,bottom: 8.0, top:10.0 ),child:Text(title, style: TextStyle(color: Theme.of(context).textSelectionColor),) ,)
          ],
        ),
        Container(
          padding: EdgeInsets.only(right: 5.0, left: 5.0),
          height: 300.0,
          child: ListView.builder(

              reverse: true,
              scrollDirection: Axis.horizontal,
              itemCount: 10,
              itemBuilder: (context, index) {
                if (index >= 10)
                  return null;
                return ProductCard(name: name,des: des,price: price,off: off, asset: asset,colorchange: true,);
              }
          ),
        )
      ],
    );
  }
  final List<Entry> data1 = <Entry>[
    Entry(
      'لباس مردانه',
      <Entry>[
        Entry('جلیقه',),
        Entry('لباس راحتی'),
        Entry('پیراهن'),
      ],
    ),
    Entry(
      'کفش مردانه',
      <Entry>[
        Entry('نیم بوت'),
        Entry('بوت'),
      ],
    ),
    Entry(
      'اکسسوری مردانه',
      <Entry>[
        Entry('ساعت'),
        Entry('جاکلیدی'),
        Entry('مج بند',),
      ],
    ),
  ];
  final List<Entry> data2 = <Entry>[
    Entry(
      'لباس زنانه',
      <Entry>[
        Entry('جلیقه',),
        Entry('لباس راحتی'),
        Entry('پیراهن'),
      ],
    ),
    Entry(
      'کفش زنانه',
      <Entry>[
        Entry('نیم بوت'),
        Entry('بوت'),
      ],
    ),
    Entry(
      'اکسسوری زنانه',
      <Entry>[
        Entry('ساعت'),
        Entry('جاکلیدی'),
        Entry('مج بند',),
      ],
    ),
  ];
  final List<Entry> data3 = <Entry>[
    Entry(
      'نوزاد',
      <Entry>[
        Entry('لباس نوزاد',),
        Entry('کفش نوزاد'),
        Entry('اکسسوری نوزاد'),
      ],
    ),
    Entry(
      'دخترانه',
      <Entry>[
        Entry('لباس دخترانه'),
        Entry('کفش رخترانه'),
      ],
    ),
    Entry(
      'پسرانه',
      <Entry>[
        Entry('کفش پسرانه'),
        Entry('لباس پسرانه'),
        Entry('اکسسوری پسرانه',),
      ],
    ),
  ];

}
class Entry {
  Entry(this.title, [this.children = const <Entry>[]]);

  final String title;
  final List<Entry> children;
}

// The entire multilevel list displayed by this app.

// Displays one Entry. If the entry has children then it's displayed
// with an ExpansionTile.
class EntryItem extends StatelessWidget {
  const EntryItem(this.entry);

  final Entry entry;

  Widget _buildTiles(Entry root) {
    if (root.children.isEmpty) return ListTile(title: Text(root.title));
    return ExpansionTile(
      key: PageStorageKey<Entry>(root),
      title: Text(root.title),
      children: root.children.map(_buildTiles).toList(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildTiles(entry);
  }
}