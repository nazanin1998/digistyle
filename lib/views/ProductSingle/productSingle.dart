import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class  ProductSingle extends StatefulWidget{
  String name;
  String des;
  String price;
  String off;
  String asset;
  ProductSingle(this.name, this.des, this.price, this.off, this.asset);
  @override
  State<StatefulWidget> createState() =>ProductSingleState(name, des, price, off, asset);

}
class ProductSingleState extends State<ProductSingle>
{
  String name;
  String des;
  String price;
  String off;
  String asset;
  final List<String> slideImgList =[
    'assets/images/cloth1.jpg',
    'assets/images/cloth2.jpg',
    'assets/images/woman1.jpg',
    'assets/images/men3.jpg',
    'assets/images/men4.jpg',
    'assets/images/men5.jpg',
  ];
  int _current = 0;
  ProductSingleState(this.name, this.des, this.price, this.off, this.asset);
  @override
  Widget build(BuildContext context) {


    return Scaffold(
      appBar: AppBar(automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        elevation: 0.0,
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: 20.0),
            child: Icon(Icons.share,size:23.0,color: Theme.of(context).textSelectionColor,),
          )
          ,
           Expanded(child: Row(),),
           Center(child: Text("Addidas", style: TextStyle(fontSize:16.0,color: Theme.of(context).textSelectionColor,),),),
          Expanded(child: Row(),),

          Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: Icon(Icons.arrow_forward,size:23.0,color: Theme.of(context).textSelectionColor,),
          ),

        ],
      ),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              mySlider(),
              desCard(),
              featuresRow(),

              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Expanded(child: Row(),),
                  Padding(padding: EdgeInsets.only( bottom: 10.0),child: Text("4.0",style: TextStyle(color:Theme.of(context).textSelectionColor,fontSize: 16.0,fontWeight: FontWeight.bold),),),
                  Padding(padding: EdgeInsets.only(bottom: 10.0),child: Text("81",style: TextStyle(color:Theme.of(context).textSelectionColor,fontSize: 9)),),
                  Padding(padding: EdgeInsets.only(bottom:11.0),child: Icon(Icons.person,color:Theme.of(context).textSelectionColor,size: 15,),),
                  Expanded(child: Row(),),

                ],
              ),

              Row(
                children: <Widget>[
                  Expanded(child: Row(),),

                  Icon(Icons.star,color: Theme.of(context).cardColor,),
                  Icon(Icons.star,color: Theme.of(context).accentColor,),
                  Icon(Icons.star,color: Theme.of(context).accentColor,),
                  Icon(Icons.star,color: Theme.of(context).accentColor,),
                  Icon(Icons.star,color: Theme.of(context).accentColor,),

                  Expanded(child: Row(),),
                ],
              )
              ,Center(
                child:
                Padding(padding: EdgeInsets.only(top: 10.0, bottom: 10.0),child: Text("امتیاز به محصول",style: TextStyle( color:Color.fromRGBO(67, 202, 211, 1.0),fontSize: 16)),),
              ),

//              tabCard(),
            ],
          ),
        ),
      ),

    );
  }

  Widget tabCard(){
    double width = 100.0;
    return Card(
      color: Colors.white,
      child: DefaultTabController(initialIndex: 2,

        length: 3,
        child: Column(
          children: <Widget>[
            Container(
                constraints: BoxConstraints.expand(height: 43),
                child: TabBar(
                    tabs: [
                      Tab(
                        child: Container(
                          alignment: Alignment.center,
                          width:  width,
                          child: Text("نظرات کاربران",),
                        ),
                      ),
                      Tab(
                        child: Container(
                          alignment: Alignment.center,
                          width:  width,
                          child: Text("مشخصات",),
                        ),
                      ),Tab(
                        child: Container(
                          alignment: Alignment.center,
                          width:  width,
                          child: Text("ویژگی ها"),
                        ),
                      ),


                    ],

                    labelColor: Theme.of(context).accentColor,
                    labelPadding: EdgeInsets.symmetric(vertical: 10),
                    labelStyle: TextStyle(fontSize: 14.0,fontWeight:FontWeight.bold,fontFamily: "Yekan"),
                    unselectedLabelColor: Theme.of(context).textSelectionColor,
                    unselectedLabelStyle: TextStyle(fontSize: 14.0,fontWeight:FontWeight.bold,fontFamily: "Yekan"),
                  ),

            ),

            Expanded(
              child: Container(
                child: TabBarView(children: [

                  Container(height: 20.0,color: Colors.pink,child: Text("me"),),
                  Container(height: 20.0,color: Colors.black,child: Text("me")),
                  Container(height: 20.0,color: Colors.black54,child: Text("me")),
                ]),
              ),
            )
          ],
        ),
      ),
    );
  }
  Widget featuresRow(){
    return Padding(
      padding: EdgeInsets.only(left:10.0, right: 10.0, bottom: 10.0,),
      child: Container(
        width: MediaQuery.of(context).size.width,

        height: 90.0,
        color: Colors.white,
        child: Row(

          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.monetization_on, size: 50.0, color: Theme.of(context).textSelectionColor,),
                Padding(padding: EdgeInsets.all(10.0),child: Text("ضمانت اصل بودن کالا",style: TextStyle(fontSize: 10.0),),)
              ],
            ),
            Expanded(child: Row(),),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.airport_shuttle ,size: 50.0,color: Theme.of(context).textSelectionColor,)
,                Padding(padding: EdgeInsets.all(10.0),child: Text("تحویل سریع و ارزان",style: TextStyle(fontSize: 10.0),),)

              ],
            ),            Expanded(child: Row(),),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.shutter_speed ,size: 50.0,color: Theme.of(context).textSelectionColor,)
,                Padding(padding: EdgeInsets.all(10.0),child: Text("ضمانت 7 روزه بازگشت کالا", style: TextStyle(fontSize: 10.0),),)

              ],
            )

          ],
        )
      ),
    );
  }
  Widget desCard(){
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Card(
        shape:RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(2.0),
        ),
        color: Colors.white,
        margin: EdgeInsets.all(10.0),
        elevation: 1.0,
        child: Column(
          children: <Widget>[
            Row(

              children:<Widget>[
                Expanded(child: Row(),),
                Padding(padding: EdgeInsets.only(right: 10.0, top: 10.0),child: Text("کفش مخصوص دویدن زنانه آدیداس مدل Tubular", style: TextStyle(fontSize: 15.0, color: Theme.of(context).textSelectionColor),),),]
            ),
            Row(

                children:<Widget>[
                  Padding(padding: EdgeInsets.only(left: 10.0,top: 10.0),child: Text("1,000,000 تومان",textDirection:TextDirection.rtl,style: TextStyle( fontSize: 15.0, color: Theme.of(context).accentColor)),),]
            ),
            Row(

                children:<Widget>[
                       Padding(padding: EdgeInsets.only(left: 10.0,top: 5.0),child: Text("1,283,000 تومان",textDirection:TextDirection.rtl,style: TextStyle( decoration: TextDecoration.lineThrough,fontSize: 12.0, color: Theme.of(context).textSelectionColor)),),]
            ),
            Row(

                children:<Widget>[
                  Expanded(child: Row(),),
                Padding(padding: EdgeInsets.only(top:10.0,right: 5.0,bottom: 10.0),child: Text("راهنما",textDirection:TextDirection.rtl,style: TextStyle( fontSize: 15, color:Color.fromRGBO(67, 202, 211, 1.0))),),

                  Padding(padding: EdgeInsets.only(top:10.0,right: 10.0, bottom: 10.0),child: Text("سایز",textDirection:TextDirection.rtl,style: TextStyle(fontSize: 15, color: Colors.black)),),]

            ),
            Row(
              children: <Widget>[
                Expanded(child: Row(),),

                Card(
                    elevation: 0,
                    color: Colors.white,
                    shape:  RoundedRectangleBorder(borderRadius: BorderRadius.circular(2.0),side: BorderSide(width: 1.0,color: Theme.of(context).cardColor)),
                    child: Container(
                      height: 50.0,
                      width: 50.0,
                      child: new Center(
                        child: Text("36", style: TextStyle(color: Theme.of(context).textSelectionColor, fontSize: 14.0,),),
                      ),
                    )
                ),
                Card(
                    elevation: 0,
                    color: Colors.white,
                    shape:  RoundedRectangleBorder(borderRadius: BorderRadius.circular(2.0),side: BorderSide(width: 1.0,color: Theme.of(context).cardColor)),
                    child: Container(
                      height: 50.0,
                      width: 50.0,
                      child: new Center(
                        child: Text("39", style: TextStyle(color: Theme.of(context).textSelectionColor, fontSize: 14.0,),),
                      ),
                    )
                )
              ],
            ),

            Row(

                children:<Widget>[
                  Expanded(child: Row(),),
                  Padding(padding: EdgeInsets.only(right: 5.0,bottom: 10.0, top: 20.0),child: Text("سفید",textDirection:TextDirection.rtl,style: TextStyle( fontSize: 14, color:Theme.of(context).textSelectionColor)),),

                  Padding(padding: EdgeInsets.only(right: 10.0, bottom: 10.0, top: 20.0),child: Text("رنگ",textDirection:TextDirection.rtl,style: TextStyle(fontSize: 14, color: Colors.black)),),]

            ),

            Row(
              children: <Widget>[
                Expanded(child: Row(),),

                Padding(padding: EdgeInsets.only(right: 8.0),child: Card(
                    elevation: 0,
                    color: Colors.white,
                    shape:  RoundedRectangleBorder(borderRadius: BorderRadius.circular(2.0),side: BorderSide(width: 1.0,color: Color.fromRGBO(67, 202, 211, 1.0))),
                    child: Container(
                      height: 140.0,
                      width: 105.0,
                      child: new Center(
                          child:Image.asset(slideImgList[5], fit: BoxFit.contain)
                      ),
                    )
                ),)
              ],
            ),

            Row(
              children: <Widget>[
                Padding(padding: EdgeInsets.only(top:10.0,bottom:5.0,left:5.0),child: Card(
                    elevation: 0,
                    color:  Color.fromRGBO(67, 202, 211, 1.0),
                    shape:  RoundedRectangleBorder(borderRadius: BorderRadius.circular(3.0)),
                    child: Container(

                      height: 40.0,
                      width: (MediaQuery.of(context).size.width-50.0)/5*2,
                      child: new Center(
                          child:Row(
                            mainAxisAlignment: MainAxisAlignment.center,

                            children: <Widget>[
                              Icon(Icons.playlist_add, color: Colors.white,size: 27.0),
                              Text("بعدا میخرم",style: TextStyle(color: Colors.white))
                            ],
                          )
                      ),
                    )
                ),),
                Expanded(child: Row(),),
                Padding(padding: EdgeInsets.only(top:10.0,bottom:5.0,right: 5.0),child: Card(
                    elevation: 0,
                    color: Theme.of(context).accentColor,
                    shape:  RoundedRectangleBorder(borderRadius: BorderRadius.circular(3.0),),
                    child: Container(
                      height: 40.0,
                      width: (MediaQuery.of(context).size.width-50.0)/5*3,
                      child: new Center(
                          child:Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(Icons.add_shopping_cart, color: Colors.white,size: 25.0,),
                              Text("افزودن به سبد خرید", style: TextStyle(color: Colors.white),)
                            ],
                          )                      ),
                    )
                ),)
              ],
            )
          ],
        ),
      ),
    );
  }
  CarouselSlider getFullScreenCarousel(BuildContext mediaContext) {
    return CarouselSlider(

      height: 300.0,
      reverse: true,
      autoPlay: false,
      viewportFraction: 1.0,
      items: slideImgList.map(
            (address) {
          return Container(
            child: ClipRRect(

                borderRadius: BorderRadius.all(Radius.circular(0.0)),
                child:   Image.asset(address, fit: BoxFit.fill,width: MediaQuery.of(context).size.width,)

            ),
          );
        },
      ).toList(),
      onPageChanged: (index) {
        setState(() {
          _current = index;
        });
      },
    );
  }
  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }

    return result;
  }
  Widget mySlider(){
    return Stack(
      alignment: Alignment.bottomCenter,
      children: <Widget>[

        getFullScreenCarousel(context),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: map<Widget>(
            slideImgList,
                (index, url) {
              return Container(
                width:  _current == 5 - index?8:7.5,
                height:  _current == 5 - index?8:7.5,
                margin: EdgeInsets.symmetric(vertical: 15.0, horizontal: 5.0),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: _current == 5 - index
                        ? Theme.of(context).accentColor
                        : Theme.of(context).backgroundColor),
              );
            },
          ),
        ),
      ],
    );
  }
}