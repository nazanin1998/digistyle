import 'package:flutter/material.dart';

class CircleImage extends StatelessWidget{

  String image;
  BoxFit fit;
  double height;
  double width;
  double borderwidth;
  Color color;

  CircleImage(this.image,{this.fit = BoxFit.fill,this.width=150.0,this.height=150.0,this.borderwidth=0.0,this.color=Colors.transparent});
  @override
  Widget build(BuildContext context) {
    return ClipRRect(
        borderRadius: new BorderRadius.circular(40.0),
        child: Container(
            width: 80.0,
            height: 80.0,
            child:
            AspectRatio(
              aspectRatio: 9 / 16,
              child: Container(

                child:Image.asset(image,fit: BoxFit.fill,) ,),)

        ));
  }


}