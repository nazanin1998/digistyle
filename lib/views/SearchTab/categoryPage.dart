
import 'package:digi_style/views/SearchTab/circle_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CategoryPage extends StatefulWidget{
  String assets;
  String title;
  CategoryPage(this.assets, this.title);
  @override
  State<StatefulWidget> createState() => CategoryPageState(assets, title);
  }

class CategoryPageState extends State<CategoryPage>{
  String assets;
  String title;
  final List<List<String>> categories = [
    ["لباس مردانه","کفش مردانه","اکسسوری مردانه"],
    ["لباس زنانه","کفش زنانه","اکسسوری زنانه"],
    ["نوزاد","دخترانه","پسرانه"],
    ["کارت هدیه دیجی استایل"]
  ];

  CategoryPageState(this.assets, this.title);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).appBarTheme.color,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[

          Container(
            height: 60.0,
            child:
            Row(crossAxisAlignment:CrossAxisAlignment.end,children: <Widget>[
              Expanded(child: Row(),),
              InkWell(child:Padding(child: Icon(Icons.clear,size: 26.0,color: Theme.of(context).textSelectionHandleColor,),padding: EdgeInsets.only(right: 15.0),),
                onTap: (){
                  Navigator.of(context).pop();
                }
                ,)],),
          ),
          Padding(padding: EdgeInsets.only(top: 5.0, bottom: 10.0),child: Center(child: Hero(
            tag: 'hero-tag'+assets,
            child:CircleImage(assets),
          ),),),
          Center(
            child:
            title=="مردانه"? Text("مردانه", style: TextStyle(fontSize: 17.0,color: Theme.of(context).textSelectionHandleColor),):
            title=="زنانه"? Text("زنانه",style: TextStyle(fontSize: 17.0,color: Theme.of(context).textSelectionHandleColor),):
            title=="بچگانه"? Text("بچگانه", style: TextStyle(fontSize: 17.0,color: Theme.of(context).textSelectionHandleColor),):
            Text("کارت هدیه", style: TextStyle(fontSize: 17.0,color: Theme.of(context).textSelectionHandleColor),),)
,
          new Divider(color: Theme.of(context).cardColor,height: 25.0,thickness: 0.2,),
          catRow(categories[0][0],categories[1][0],categories[2][0],categories[3][0]),
          catRow(categories[0][1],categories[1][1],categories[2][1],""),
          catRow(categories[0][2],categories[1][2],categories[2][2],""),
    ])
    );
  }
  Widget catRow(String t1, String t2,String t3,String t4){
    return Padding(padding: EdgeInsets.only(bottom: 17.0,top: 17.0),child: Center(
      child:
      title=="مردانه"? Text(t1, style: TextStyle(fontSize: 16.0,color: Theme.of(context).textSelectionHandleColor),):
      title=="زنانه"? Text(t2 ,style: TextStyle(fontSize: 16.0,color: Theme.of(context).textSelectionHandleColor),):
      title=="بچگانه"? Text(t3, style: TextStyle(fontSize: 16.0,color: Theme.of(context).textSelectionHandleColor),):
      Text(t4, style: TextStyle(fontSize: 16.0,color: Theme.of(context).textSelectionHandleColor),),)
      ,);}

}