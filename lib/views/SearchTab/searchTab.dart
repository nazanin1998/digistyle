
import 'package:digi_style/views/SearchTab/categoryPage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'circle_image.dart';

class SearchTab extends StatefulWidget{

  @override
  State<StatefulWidget> createState() =>SearchTabState() ;

}

class SearchTabState extends State<SearchTab> {
  final myController = TextEditingController();

  bool focusOnCard= false;
  final List<String> titles =["مردانه","زنانه","بچگانه","کارت هدیه"];
  final List<String> assets =["assets/images/men1.jpg","assets/images/woman.jpg","assets/images/kid.jpg","assets/images/cloth1.jpg"];
  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    myController.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    setState(() {

    });
    return Scaffold(
        backgroundColor: Theme.of(context).appBarTheme.color,
        body: Stack(
          children: <Widget>[
            Column(
              children: <Widget>[
                Padding(padding: EdgeInsets.only(top: 115.0 ),child: Row(children: <Widget>[

                  Expanded(child: Row(),),
                  Padding(padding: EdgeInsets.only(left:15.0, right: 15.0 ,bottom: 8.0, top:10.0 ),child:Text("دسته بندی", style: TextStyle(color:Colors.white),) ,)

                ],), ),



                Expanded(
                  child: GridView.count(

                    crossAxisCount: 2,
                    childAspectRatio: (2 / 2),
                    children: List.generate(4, (index) {
                      return  SearchCard(assets[index],titles[index]);
                    }),
                  ),
                )
              ],
            ),
            focusOnCard?InkWell(
              onTap: (){
                setState(() {
                  print("hey");
                  focusOnCard = false;
                });
              },child: Container(
              color: Colors.black54,
              height:  MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
            ),
            ):Container(),
            Column(
              children: <Widget>[
                InkWell(
                  child:Card(

                    margin: EdgeInsets.only(left: 8.0,top: 30.0,bottom: 32.0,right: 8.0),
                    child:
                    TextField(
                      onTap: (){
                        setState(() {
                          print("hey");
                          focusOnCard = true;
                        });
                      },onSubmitted: (str){
                      setState(() {
                        print("done");
                        focusOnCard = false;
                      });
                    },
                        textAlign: TextAlign.right,
                        textInputAction: TextInputAction.search,
                        controller: myController,
                        decoration: InputDecoration(
                            prefixIcon:IconButton(
                              icon: Icon(Icons.keyboard_voice),
                            ),
                            contentPadding: EdgeInsets.all(12.0),
                            border: InputBorder.none,
                            hintText: '...جستجو')
                    ),

                  ) ,

                )
              ],
            ),


          ],
        )
    );
  }



}
class SearchCard extends StatelessWidget
{
  String title;
  String assets;
  SearchCard(this.assets, this.title);
  @override
  Widget build(BuildContext context) {
    return
      Center(
        child: GestureDetector(
          onTap: () {
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (context) =>CategoryPage(assets,title)));

          },
          child:Container(
            child:Column(children: <Widget>[
              Hero(
                tag: 'hero-tag'+assets,
                child:CircleImage(assets),
              ),

//              CircleImage(assets),
              Padding(child: Text(title, style: TextStyle(color: Theme.of(context).textSelectionHandleColor),),padding:EdgeInsets.only(top: 15),)
            ],



            ),)
        ),
      );
  }
}